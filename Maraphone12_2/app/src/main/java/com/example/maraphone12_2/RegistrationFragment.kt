package com.example.maraphone12_2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView


class RegistrationFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        activity?.findViewById<BottomNavigationView>(R.id.bottomNavView)?.visibility = View.GONE
        val view =  inflater.inflate(R.layout.fragment_registration, container, false)
        view.findViewById<Button>(R.id.btnToRegs).setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_registrationFragment_to_loginFragment2)
        }
        return view
    }

    companion object {

        fun newInstance(param1: String, param2: String) =
            RegistrationFragment()
    }
}